# To-Do List

This program tracks your tasks and deadlines. Unlike alternatives, this program is designed for use in a terminal. It is lightweight and offers great flexibility. All tasks are kept in a single database file for easy synchronization with a third party tool like Nextcloud or Dropbox.

![Screenshot](screenshots/interface.png "Main Interface")

**Note:**
For more information, please visit the [official documentation](https://caton101.gitlab.io/todo-documentation/).

## Dependencies

- A [Linux distribution](https://en.wikipedia.org/wiki/List_of_Linux_distributions)
- A [terminal emulator](https://en.wikipedia.org/wiki/List_of_terminal_emulators) (xterm, Gnome Terminal, Konsole, etc.)
- [Python 3](https://www.python.org/downloads/) or higher
- The [blessed](https://pypi.org/project/blessed/) Python Package

## Installation

1. Clone this repository:
`git clone https://gitlab.com/caton101/todo-list`

2. Enter the downloaded directory:
`cd todo-list`

3. Mark the install script as executable:
`chmod +x install`

4. Run the installer:
`./install`

## FAQ

**Can I use this program on Windows?**

Yes. You need to install [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10), and then follow the installation instructions inside a Linux environment.

**Can I use this program on MacOS?**

To-Do List does not officially support macOS, but it should be possible. Follow the install instructions inside the Terminal app.

**Can I use this program on [random operating system] instead of Linux?**

To-Do List only supports Linux, but it might be possible. You need to have BASH, a Python environment, a UNIX-like file structure, and UNIX-like commands such as `chmod`, `ln` and `rm`.

**Can I embed this program inside my project?**

Yes, but you must be in compliance with the software license.

## Troubleshooting

Read the [documentation](https://caton101.gitlab.io/todo-documentation/) **first** for troubleshooting and questions. If the documentation does not help you, please [create an issue](https://gitlab.com/caton101/todo-list/-/issues).

## How To Contribute

First of all, thank you for your interest in this program. This project employs KDE's ["scratch your own itch"](https://community.kde.org/Get_Involved/development#Choose_what_to_do) policy. Create an issue for something that bothers you (in either the documentation or the source code) and create a pull request to fix the issue. If you do not have an itch, try fixing an issue tagged with [`Good First Issue`](https://gitlab.com/caton101/todo-list/-/issues?label_name%5B%5D=Good+First+Issue).

## Licensing

This program is licensed under the BSD 2-Clause License. For more info, please read the `LICENSE` file or visit [this link](https://opensource.org/licenses/BSD-2-Clause).

## Usage

### Launching

- Install the program
- Open a terminal
- run the `todo` command

### Keyboard Shortcuts

Task List:

| Key        | Description                            |
| :--------- | :------------------------------------- |
| Up Arrow   | Move the selection bar up one task     |
| Down Arrow | Move the selection bar down one task   |
| Delete     | Remove a task from the list            |
| Tab        | Toggle the completion status of a task |
| Enter      | Modify a task                          |
| Escape     | Exit the program                       |

Modifying a task:

| Key        | Description                            |
| :--------- | :------------------------------------- |
| Up Arrow   | Move the selection bar up one option   |
| Down Arrow | Move the selection bar down one option |
| Enter      | Change the selected option             |
